import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QueryBarComponent } from './queryBar/queryBar.component';
import { NewsFeedComponent } from './newsFeed/newsFeed.component';
import { CategoriesComponent } from './categories/categories.component';
import { HttpClientModule } from "@angular/common/http"
import { NewsFeedItemComponent } from './newsFeedItem/newsFeedItem.component';
import { NgxPaginationModule } from "ngx-pagination";

@NgModule({
   declarations: [
      AppComponent,
      QueryBarComponent,
      NewsFeedComponent,
      CategoriesComponent,
      NewsFeedItemComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      NgxPaginationModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
