import { Component, OnInit } from '@angular/core';
import { NewsService, ResponseObject, ResponseArticle } from "../News.service";
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-newsFeed',
  templateUrl: './newsFeed.component.html',
  styleUrls: ['./newsFeed.component.css']
})
export class NewsFeedComponent implements OnInit {
  recentNews: ResponseObject;
  config: any;
  p: number = 1;

  constructor(private news: NewsService) {
    this.config = {
      itemsPerPage: 10,
      currentPage: this.p,
      totalItems: 100
    }
  }

  pageChange(event){
    console.log("Changing page")
    this.config.currentPage = event
  }

  ngOnInit() {
    this.news.getRecentNews().subscribe((data: ResponseObject) => {
      this.recentNews = data;
      console.log("donee")
    })
    
    

  }

}
