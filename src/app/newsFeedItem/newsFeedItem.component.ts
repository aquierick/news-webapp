import { Component, OnInit, Input } from '@angular/core';
import { ResponseArticle } from "../News.service";
@Component({
  selector: 'app-newsFeedItem',
  templateUrl: './newsFeedItem.component.html',
  styleUrls: ['./newsFeedItem.component.css']
})
export class NewsFeedItemComponent implements OnInit {
  @Input() article: ResponseArticle;

  constructor() { }

  ngOnInit() {}

  getDescription() {
    let maxLength = 120

    if (this.article.description) {
      if (this.article.description.length <= maxLength) {
        return this.article.description
      } else {
        return this.article.description.substring(0, maxLength - 3) + "..."
      }
    } else {
      return ""
    }
  }

  getTitle() {
    let maxLength = 100

    if (this.article.title) {
      if (this.article.title.length <= maxLength) {
        return this.article.title
      } else {
        return this.article.title.substring(0, maxLength - 3) + "..."
      }
    } else {
      return ""
    }
  }

}
