import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

export interface ResponseObject {
  "status": String,
  "totalResults": Number,
  "articles": [ResponseArticle]
}

export interface ResponseArticle {
  "source": {
    "id": String,
    "name": String
  },
  "author": String,
  "title": String,
  "description": String,
  "url": String,
  "urlToImage": String,
  "publishedAt": String,
  "content": String
}

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private key = "53381491edcd476b9248a4023f1e5f58"
  private responseData: ResponseObject

  constructor(private http: HttpClient) {
    this.responseData = null
  }

  getRecentNews() {
    let requestURL = "https://newsapi.org/v2/top-headlines?q= &pagesize=100&language=en&from=" + this.getFormattedDate() + "&sortBy=publishedAt&apiKey=" + this.key
    return this.http.get<ResponseObject>(requestURL)
  }

  private getFormattedDate() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();

    return yyyy + "-" + mm + "-" + dd
    
  }



}
