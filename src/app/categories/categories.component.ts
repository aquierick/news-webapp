import { Component, OnInit } from '@angular/core';
import { NewsService, ResponseArticle, ResponseObject } from "../News.service";
import { stopWords } from "../stopWords";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  term1 
  term2 
  term3
  term4
  

  constructor(private news: NewsService) { }

  articles: [ResponseArticle]
  ngOnInit() {
    this.news.getRecentNews().subscribe((data: ResponseObject) => {
      this.articles = data.articles
      var termFrequency = {}

      var first = {"term": "", frequency:4};
      var second = {"term": "", frequency:3};
      var third = {"term": "", frequency:2};
      var fourth = {"term": "", frequency:1};

      for (let article of this.articles){
        article.title = article.title.replace(",", "")
        article.title = article.title.replace("'", "")
        article.title = article.title.replace("&", "")
        for (let word of article.title.split(" ")){
          word = word.toLowerCase()
          if (!stopWords.includes(word.toLowerCase())) {
            if (termFrequency[word]){
              termFrequency[word] = termFrequency[word] + 1
              if (termFrequency[word] > first.frequency) {
                first.term = word
                first.frequency = termFrequency[word]
              }
              else if (termFrequency[word] > second.frequency){
                second.term = word
                second.frequency = termFrequency[word]
              }
              else if (termFrequency[word] > third.frequency){
                third.term = word
                third.frequency = termFrequency[word]
              }
              else if (termFrequency[word] > fourth.frequency){
                fourth.term = word
                fourth.frequency = termFrequency[word]
              }
            }
            else {
              termFrequency[word] = 1
            }
          }
        }
      }

      console.log(termFrequency)

      this.term1 = first
      this.term2 = second
      this.term3 = third
      this.term4 = fourth

    })
  }

}
